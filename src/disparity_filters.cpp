/*
 *  Copyright 2014 Néstor Morales Hernández <nestor@isaatc.ull.es>
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
#include <pluginlib/class_list_macros.h>
#include "disparity_filters.h"
#include "opencvtools.h"

#include <opencv2/opencv.hpp>

#include <cv_bridge/cv_bridge.h>

#include <sensor_msgs/image_encodings.h>

#include <iostream>
#include <ros/node_handle.h>
#include <sensor_msgs/image_encodings.h>
#include <cv_bridge/cv_bridge.h>

using namespace std;

// watch the capitalization carefully
PLUGINLIB_DECLARE_CLASS(disparity_filters, DisparityFiltersNodelet, 
                        disparity_filters::DisparityFiltersNodelet, nodelet::Nodelet) 

namespace disparity_filters
{

void DisparityFiltersNodelet::onInit() 
{
    NODELET_INFO("Initializing nodelet...");
    
    ros::NodeHandle local_nh("~");
    ros::NodeHandle nh;
    
    int threads, dispCount;
    nh.param("threads", threads, 4);
    nh.param("disp_count", dispCount, 64);
    m_threads = threads;
    m_dispCount = dispCount;
    
    nh.param("despeckle_filter", m_despeckleFilter, true);
    nh.param("min_speckle_segment_size", m_minSpeckleSegmentSize, 100);
    nh.param("speckle_sim_threshold", m_speckleSimThreshold, 1.0);
    nh.param("adaptive_mean_filter", m_adaptiveMeanFilter, true);
    nh.param("gap_filter", m_gapFilter, false);
    nh.param("block_filter", m_blockFilter, false);
    nh.param("block_width", m_blockWidth, 20);
    nh.param("block_height", m_blockHeight, 20);
    
    image_transport::ImageTransport local_it(local_nh);
    
    m_disparityImageSub.subscribe(local_it, "disparity_input", 1);
    m_leftImageSub.subscribe(local_it, "left/image", 1);
    m_left_info_sub.subscribe(local_nh, "left/camera_info", 1);
    m_right_info_sub.subscribe(local_nh, "right/camera_info", 1);
    
    m_depthImagePub = local_it.advertise("depth", 1);
    m_disparityImagePub = local_it.advertise("disparity", 1);
    m_pointCloudPub = local_nh.advertise<sensor_msgs::PointCloud2> ("point_cloud", 1);
    
    // Synchronize input topics. Optionally do approximate synchronization.
    bool approx;
    int queue_size;
    nh.param("approximate_sync", approx, true);
    nh.param("queue_size", queue_size, 10);
    if (approx)
    {
        m_approximate_sync.reset(new ApproximateSync(ApproximatePolicy(queue_size),
                                                     m_disparityImageSub, m_leftImageSub, m_left_info_sub, m_right_info_sub) );
        m_approximate_sync->registerCallback(boost::bind(&DisparityFiltersNodelet::callBack, this, _1, _2, _3, _4));
    }
    else
    {
        m_exact_sync.reset(new ExactSync(ExactPolicy(queue_size),
                                         m_disparityImageSub, m_leftImageSub, m_left_info_sub, m_right_info_sub) );
        m_exact_sync->registerCallback(boost::bind(&DisparityFiltersNodelet::callBack, this, _1, _2, _3, _4));
    }

    ROS_INFO("PARAMS INFO");
    ROS_INFO("===========");
    ROS_INFO("threads: %d", m_threads);
    ROS_INFO("disp_count: %d", m_dispCount);
    ROS_INFO("despeckle_filter %d", m_despeckleFilter);
    ROS_INFO("min_speckle_segment_size %d", m_minSpeckleSegmentSize);
    ROS_INFO("speckle_sim_threshold %f", m_speckleSimThreshold);
    ROS_INFO("adaptive_mean_filter %d", m_adaptiveMeanFilter);
    ROS_INFO("gap_filter %d", m_gapFilter);
    ROS_INFO("approximate_sync %d", approx);
    ROS_INFO("queue_size %d", queue_size);
}

void DisparityFiltersNodelet::callBack(const sensor_msgs::ImageConstPtr& inputMsg,
                                       const sensor_msgs::ImageConstPtr& leftImgMsg,
                                       const sensor_msgs::CameraInfoConstPtr& l_info_msg, 
                                       const sensor_msgs::CameraInfoConstPtr& r_info_msg)
{
    boost::mutex::scoped_lock lock(m_mutex);
    
    INIT_CLOCK(startCompute)
    
    cv_bridge::CvImagePtr cv_ptr;
    try {
        cv_ptr = cv_bridge::toCvCopy(inputMsg, sensor_msgs::image_encodings::TYPE_32FC1);
    } catch (cv_bridge::Exception& e) {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }
    
    float * data = (float*)_mm_malloc(inputMsg->width * inputMsg->height * sizeof(float), 32);
    memcpy(data, &cv_ptr->image.data[0], inputMsg->width * inputMsg->height * sizeof(float));

    if (m_despeckleFilter) despeckleFilter(data, data, inputMsg->width, inputMsg->height);
    if (m_gapFilter) gapFilter(data, data, inputMsg->width, inputMsg->height);
    if (m_adaptiveMeanFilter) adaptMean(data, data, inputMsg->width, inputMsg->height);
    if (m_blockFilter) blockFilter(data, data, inputMsg->width, inputMsg->height);
    
    publishOutputMaps(inputMsg, data, l_info_msg, r_info_msg);

    publish_point_cloud(leftImgMsg, data, l_info_msg, r_info_msg);

    END_CLOCK(totalCompute, startCompute)
    NODELET_INFO("[%s] Total filtering: %f seconds", __FILE__, totalCompute);
}

void DisparityFiltersNodelet::despeckleFilter(const float* source, float* dest, 
                                              const uint32_t & width, const uint32_t &height)
{
    INIT_CLOCK(startCompute)
    std::vector<cv::Point3i> segment;
    
    std::vector<unsigned int> aux;
    aux.resize(width * height);
    std::fill(aux.begin(),  aux.begin() + width * height, 0);
    
    int index = 0;
    for(uint32_t i = 0; i < height; i++) {
        for(uint32_t j = 0; j < width; j++, index++) {
            if(!aux[index]) {
                aux[index] = 1;
                segment.clear();
                segment.push_back(cv::Point3i(j, i, index));
                for(uint32_t k = 0; k < segment.size(); k++) {
                    cv::Point3i curr = segment[k];
                    
                    cv::Point3i neigh(curr.x + 1, curr.y, curr.z + 1);
                    if(curr.x + 1 < (int) width && !aux[neigh.z] && 
                        fabs(source[neigh.z] - source[curr.z]) <= m_speckleSimThreshold) {
                        
                        segment.push_back(neigh);
                        aux[neigh.z] = 1;
                    }
                        
                    neigh = cv::Point3i(curr.x - 1, curr.y, curr.z - 1);
                    if(curr.x - 1 >= 0 && !aux[neigh.z] && 
                        fabs(source[neigh.z] - source[curr.z]) <= m_speckleSimThreshold) {
                        
                        segment.push_back(neigh);
                        aux[neigh.z] = 1;
                    }
                        
                    neigh = cv::Point3i(curr.x, curr.y + 1, curr.z + width);
                    if(curr.y + 1 < (int) height && !aux[neigh.z] && 
                        fabs(source[neigh.z] - source[curr.z]) <= m_speckleSimThreshold) {
                        
                        segment.push_back(neigh);
                        aux[neigh.z] = 1;
                    }
                            
                    neigh = cv::Point3i(curr.x, curr.y - 1, curr.z - width);
                    if(curr.y - 1 >= 0 && !aux[neigh.z] &&
                        fabs(source[neigh.z] - source[curr.z]) <= m_speckleSimThreshold) {
                            
                        segment.push_back(neigh);
                        aux[neigh.z] = 1;
                    }
                }
                
                if (segment.size() < m_minSpeckleSegmentSize) {
                    for(uint32_t k = 0; k < segment.size(); k++) {
                        dest[segment[k].z] = 0;
                    }
                }
            }
        }
    }       
    
    END_CLOCK(totalCompute, startCompute)
    NODELET_INFO("[%s] Despeckle Filter: %f seconds", __FILE__, totalCompute);
    
    m_mutex.unlock();
}

void DisparityFiltersNodelet::adaptMean(float* source, float* dest, const uint32_t& width, const uint32_t& height)
{
    INIT_CLOCK(startCompute)
    #pragma omp parallel num_threads(m_threads)
    {
        int idThread = omp_get_thread_num();

        __attribute__((aligned(16))) float val[8], weight[4], factor[4];
        
        int32_t* ival = (int32_t*) val;
        ival[0] = 0x7FFFFFFF;
        ival[1] = 0x7FFFFFFF;
        ival[2] = 0x7FFFFFFF;
        ival[3] = 0x7FFFFFFF;
        __m128 xabsmask = _mm_load_ps(val);
        
        __m128 xconst0 = _mm_set1_ps(0);
        __m128 xconst4 = _mm_set1_ps(4);
        __m128 xval, xweight1, xweight2, xfactor1, xfactor2;
        
        int32_t k0 = (width * height * idThread) / m_threads;
        int32_t k1 = (height * height * (idThread + 1)) / m_threads;
        
        std::copy(source + k0, source + k1, dest + k0);
        
        #pragma omp barrier
        
        k0 = ((height - 6) * idThread) / m_threads + 3;
        k1 = ((height - 6) * (idThread + 1)) / m_threads + 3;
        
        {
            
            int stride = k0 * width;
            for(int32_t i = k0; i < k1; i++, stride += width) {
                
                for(int32_t j = 0; j < 7; j++) {
                    val[j] = * (source + stride + j);
                }
                
                for(int32_t j = 7; j < (int) width; j++) {
                    
                    float val_curr = * (source + stride + j - 3);
                    val[j % 8] = * (source + stride + j);
                    
                    xval     = _mm_load_ps(val);
                    xweight1 = _mm_sub_ps(xval, _mm_set1_ps(val_curr));
                    xweight1 = _mm_and_ps(xweight1, xabsmask);
                    xweight1 = _mm_sub_ps(xconst4, xweight1);
                    xweight1 = _mm_max_ps(xweight1, xconst0);
                    xfactor1 = _mm_mul_ps(xval, xweight1);
                    
                    xval     = _mm_load_ps(val + 4);
                    xweight2 = _mm_sub_ps(xval, _mm_set1_ps(val_curr));
                    xweight2 = _mm_and_ps(xweight2, xabsmask);
                    xweight2 = _mm_sub_ps(xconst4, xweight2);
                    xweight2 = _mm_max_ps(xconst0, xweight2);
                    xfactor2 = _mm_mul_ps(xval, xweight2);
                    
                    xweight1 = _mm_add_ps(xweight1, xweight2);
                    xfactor1 = _mm_add_ps(xfactor1, xfactor2);
                    
                    _mm_store_ps(weight, xweight1);
                    _mm_store_ps(factor, xfactor1);
                    
                    float weight_sum = weight[0] + weight[1] + weight[2] + weight[3];
                    float factor_sum = factor[0] + factor[1] + factor[2] + factor[3];
                    if(weight_sum > 0 && factor_sum >= 0) {
                        * (dest + stride + j - 3) = factor_sum / weight_sum;
                    }
                }
            }
            
            #pragma omp barrier
            
            int width3 = 3 * width;
            k0 = ((width - 6) * idThread) / m_threads + 3;
            k1 = ((width - 6) * (idThread + 1)) / m_threads + 3;
            for(int32_t j = k0; j < k1; j++) {
                
                int stride = 0;
                for(int32_t i = 0; i < 7; i++, stride += width) {
                    val[i] = * (dest + stride + j);
                }
                
                
                stride = 7 * width;
                
                for(int32_t i = 7; i < (int) height; i++, stride += width) {
                    
                    float val_curr = * (dest + stride + j - width3);
                    val[i % 8] = * (dest + stride + j);
                    
                    xval     = _mm_load_ps(val);
                    xweight1 = _mm_sub_ps(xval, _mm_set1_ps(val_curr));
                    xweight1 = _mm_and_ps(xweight1, xabsmask);
                    xweight1 = _mm_sub_ps(xconst4, xweight1);
                    xweight1 = _mm_max_ps(xweight1, xconst0);
                    xfactor1 = _mm_mul_ps(xval, xweight1);
                    
                    xval     = _mm_load_ps(val + 4);
                    xweight2 = _mm_sub_ps(xval, _mm_set1_ps(val_curr));
                    xweight2 = _mm_and_ps(xweight2, xabsmask);
                    xweight2 = _mm_sub_ps(xconst4, xweight2);
                    xweight2 = _mm_max_ps(xweight2, xconst0);
                    xfactor2 = _mm_mul_ps(xval, xweight2);
                    
                    xweight1 = _mm_add_ps(xweight1, xweight2);
                    xfactor1 = _mm_add_ps(xfactor1, xfactor2);
                    
                    _mm_store_ps(weight, xweight1);
                    _mm_store_ps(factor, xfactor1);
                    
                    float weight_sum = weight[0] + weight[1] + weight[2] + weight[3];
                    float factor_sum = factor[0] + factor[1] + factor[2] + factor[3];
                    if(weight_sum > 0 && factor_sum >= 0) {
                        * (source + stride + j - width3) = factor_sum / weight_sum;
                    }
                }
            }
        }

        #pragma omp barrier
    }
    END_CLOCK(totalCompute, startCompute)
    NODELET_INFO("[%s] AdaptMean: %f seconds", __FILE__, totalCompute);
}

void DisparityFiltersNodelet::gapFilter(float* source, float* dest, const uint32_t& width, const uint32_t& height)
{
    INIT_CLOCK(startCompute)
    #pragma omp parallel num_threads(m_threads)
    {
        int idThread = omp_get_thread_num();

        const float discon_threshold = 3.0;
        int32_t ipol_gap_width =3 ;


        uint32_t k0 = (height * idThread) / m_threads;
        uint32_t k1 = (height * (idThread + 1)) / m_threads;

        dest = (float*) source + k0 * width;
        for(uint32_t i = k0; i < k1; i++) {
            int count = 0;
            for(int32_t j = 0; j < (int) width; j++, dest++) {
                float d = *dest;
                if(d >= 0) {
                    if(count >= 1 && count <= ipol_gap_width && j > count) {
                        float d1 = dest[-count - 1];
                        float dmean = (fabs(d1 - d) < discon_threshold) ? (d1 + d) / 2 : std::min(d1, d);
                        int index2 = 0;
                        for(int32_t u_curr = j - count; u_curr < j; u_curr++) {
                            dest[--index2] = dmean;
                        }
                    }
                    count = 0;
                } else {
                    count++;
                }
            }
        }

        #pragma omp barrier

        dest = source;
        k0 = (width * idThread) / m_threads;
        k1 = (width * (idThread + 1)) / m_threads;
        for(uint32_t j = k0; j < k1; j++) {
            int count = 0;
            int stride = 0;
            for(int32_t i = 0; i < (int) height; i++, stride += width) {
                int index = stride + j;
                float d = dest[index];
                if(d >= 0) {
                    if(count >= 1 && count <= ipol_gap_width && i > count) {
                        float d1 = dest[index - (count + 1) * width];
                        float dmean = (fabs(d1 - d) < discon_threshold) ? (d1 + d) / 2 : std::min(d1, d);
                        int index2 = index - width;
                        for(int32_t u_curr = i - count; u_curr < i; u_curr++, index2 -= width) {
                            dest[index2] = dmean;
                        }
                    }
                    count = 0;
                } else {
                    count++;
                }
            }
        }
    }

    #pragma omp barrier

    END_CLOCK(totalCompute, startCompute)
    NODELET_INFO("[%s] Gap filter: %f seconds", __FILE__, totalCompute);
}
    
void DisparityFiltersNodelet::blockFilter(const float* source, float* dest, 
                                          const uint32_t& width, const uint32_t& height)
{
    INIT_CLOCK(startCompute)
    
    cv::Mat imgIn(height, width, CV_32FC1);
    memcpy(&imgIn.data[0], source, width * height * sizeof(float));
    
    /// Establish histogram parameters
    const bool isUniform = true;
    const bool accumulate = false;
    const float range[] = { 0, 128 } ;
    const float* histRange = { range };
    
    #pragma omp parallel for num_threads(m_threads)
    for (uint32_t u = 0; u < width; u += m_blockWidth) {
        for (uint32_t v = 0; v < height; v += m_blockHeight) {
            
            const uint32_t w = std::min(width - u, (uint32_t)m_blockWidth);
            const uint32_t h = std::min(height - v, (uint32_t)m_blockHeight);
            
            cv::Mat roi = imgIn(cv::Rect(u, v, w, h));
            
            const int & histSize = m_dispCount;
            cv::Mat histogram;
            cv::calcHist(&roi, 1, 0, cv::Mat(), histogram, 1, &histSize, &histRange, isUniform, accumulate);
            
            double minVal, maxVal;
            int threshValue;
            cv::minMaxIdx(histogram, &minVal, &maxVal, 0, &threshValue);

            cv::threshold(roi, roi, threshValue - 2.0, 0.0, cv::THRESH_TOZERO);
            cv::threshold(roi, roi, threshValue + 2.0, threshValue, cv::THRESH_TOZERO_INV);
        }
    }
    
    memcpy(dest, &imgIn.data[0], width * height * sizeof(float));

    END_CLOCK(totalCompute, startCompute)
    NODELET_INFO("[%s] Block filter: %f seconds", __FILE__, totalCompute);
}

void DisparityFiltersNodelet::publishOutputMaps(const sensor_msgs::ImageConstPtr& inputMsg, const float * dispData,
                                                const sensor_msgs::CameraInfoConstPtr& l_info_msg, 
                                                const sensor_msgs::CameraInfoConstPtr& r_info_msg)
{
    bool publishDisparity = (m_disparityImagePub.getNumSubscribers() != 0);
    bool publishDepth = (m_depthImagePub.getNumSubscribers() != 0);
    
    image_geometry::StereoCameraModel model;
    model.fromCameraInfo(*l_info_msg, *r_info_msg);
    
    const int32_t & width = inputMsg->width;
    const int32_t & height = inputMsg->height;
    
    cv_bridge::CvImage disparityMsg, depthMsg;
    if (publishDisparity) {
        disparityMsg.header = inputMsg->header;
        disparityMsg.encoding = sensor_msgs::image_encodings::TYPE_32FC1;
        disparityMsg.image = cv::Mat(height, width, CV_32FC1);
    }
    if (publishDepth) {
        depthMsg.header = inputMsg->header;
        depthMsg.encoding = sensor_msgs::image_encodings::TYPE_32FC1;
        depthMsg.image = cv::Mat(height, width, CV_32FC1);
    }
    
    #pragma omp num_threads(m_threads)
    for (uint32_t v = 0, i = 0; v < height; v++)  {
        for (uint32_t u = 0; u < width; u++, i++)  {
            if (dispData[i] > 0) {
                if (publishDisparity) disparityMsg.image.at<float>(v, u) = std::max(dispData[i], 0.0f);
                if (publishDepth) depthMsg.image.at<float>(v, u) = std::max(model.getZ(dispData[i]), 0.0);
            }
        }
    }
    
    // Publish
    if (publishDisparity) {
        m_disparityImagePub.publish(disparityMsg.toImageMsg());
    }
    if (publishDepth) {
        m_depthImagePub.publish(depthMsg.toImageMsg());
    }
}

void DisparityFiltersNodelet::publish_point_cloud(const sensor_msgs::ImageConstPtr& l_image_msg,
                                                  float * l_disp_data,
                                                  const sensor_msgs::CameraInfoConstPtr& l_info_msg,
                                                  const sensor_msgs::CameraInfoConstPtr& r_info_msg)
{
//     if (m_pointCloudPub.getNumSubscribers() == 0)
//         return;

    try
    {
        const cv::Mat leftImg = (cv_bridge::toCvShare(l_image_msg, sensor_msgs::image_encodings::RGB8))->image;

        const int32_t & l_width = l_image_msg->width;
        const int32_t & l_height = l_image_msg->height;

        image_geometry::StereoCameraModel model;
        model.fromCameraInfo(*l_info_msg, *r_info_msg);

        PointCloud::Ptr point_cloud(new PointCloud());
        point_cloud->width = 1;
        point_cloud->points.reserve(l_width * l_height);

        for (uint32_t u = 0; u < l_width; u ++) {
            for (uint32_t v = 0; v < l_height; v ++) {
                uint32_t index = v * l_width + u;

                if (l_disp_data[index] > 1) {

                    cv::Point2d left_uv;
                    left_uv.x = u;
                    left_uv.y = v;

                    cv::Point3d point;
                    model.projectDisparityTo3d(left_uv, l_disp_data[index], point);

                    PointType pointPCL;

                    pointPCL.x = point.x;
                    pointPCL.y = point.y;
                    pointPCL.z = point.z;
                    const cv::Vec3b & pointColor = leftImg.at<cv::Vec3b>(v, u);
                    pointPCL.r = pointColor[0];
                    pointPCL.g = pointColor[1];
                    pointPCL.b = pointColor[2];

                    point_cloud->push_back(pointPCL);
                }
            }
        }

        sensor_msgs::PointCloud2 cloudMsg;
        pcl::toROSMsg (*point_cloud, cloudMsg);
        cloudMsg.header.frame_id = l_info_msg->header.frame_id;
        cloudMsg.header.stamp = ros::Time::now();
        cloudMsg.header.seq = l_info_msg->header.seq;

        m_pointCloudPub.publish(cloudMsg);
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
    }
}

}
