/*
 *  Copyright 2014 Néstor Morales Hernández <nestor@isaatc.ull.es>
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef OPENCVTOOLS_H
#define OPENCVTOOLS_H

#include <opencv2/opencv.hpp>

using namespace cv;

class OpenCVTools
{
public:
    static cv::Mat getColoredDisparityMap(const cv::Mat & imgIn);
    static cv::Mat showHistogram(const Mat& histogram, const uint32_t & histSize);
};

#endif // OPENCVTOOLS_H
