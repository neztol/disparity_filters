/*
 *  Copyright 2014 Néstor Morales Hernández <nestor@isaatc.ull.es>
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "opencvtools.h"

#include <iostream>

using namespace std;
using namespace cv;

cv::Mat OpenCVTools::getColoredDisparityMap(const cv::Mat& imgIn)
{
    double min;
    double max;
    cv::minMaxIdx(imgIn, &min, &max);
    cout << "Min " << min << endl;
    cout << "Max " << max << endl;
    cv::Mat adjMap, falseColorsMap;
    imgIn.convertTo(adjMap, CV_8UC1, 255 / (max-min), -min); 
    cv::applyColorMap(adjMap, falseColorsMap, cv::COLORMAP_AUTUMN);
    
    return falseColorsMap;
}

cv::Mat OpenCVTools::showHistogram(const Mat& histogram, const uint32_t & histSize)
{
    // Draw the histograms for B, G and R
    int hist_w = 512; int hist_h = 400;
    int bin_w = cvRound( (double) hist_w / histSize );
    
    Mat histImage( hist_h, hist_w, CV_8UC3, Scalar::all(0) );
    
    /// Normalize the result to [ 0, histImage.rows ]
    normalize(histogram, histogram, 0, histImage.rows, NORM_MINMAX, -1, Mat() );
    
    /// Draw for each channel
    for( int i = 1; i < histogram.rows; i++ ) {
        line( histImage, Point( bin_w*(i-1), hist_h - cvRound(histogram.at<float>(i-1)) ) ,
              Point( bin_w*(i), hist_h - cvRound(histogram.at<float>(i)) ),
              Scalar( 0, 255, 0), 2, 8, 0  );
    }
    
    return histImage;
}
