/*
 *  Copyright 2014 Néstor Morales Hernández <nestor@isaatc.ull.es>
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include <nodelet/nodelet.h>

#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/sync_policies/approximate_time.h>

#include <image_transport/subscriber_filter.h>

#include <image_geometry/stereo_camera_model.h>

#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>

#include <omp.h>
#include <boost/thread/pthread/mutex.hpp>

#define INIT_CLOCK(start) double start = omp_get_wtime();
#define RESET_CLOCK(start) start = omp_get_wtime();
#define END_CLOCK(time, start) float time = omp_get_wtime() - start;
#define END_CLOCK_2(time, start) float time = omp_get_wtime() - start;

namespace disparity_filters {
    
class DisparityFiltersNodelet : public nodelet::Nodelet {
public:
    void onInit();
    
protected:
    void callBack(const sensor_msgs::ImageConstPtr& inputMsg,
                  const sensor_msgs::ImageConstPtr& leftImgMsg,
                  const sensor_msgs::CameraInfoConstPtr& l_info_msg,
                  const sensor_msgs::CameraInfoConstPtr& r_info_msg);
    void adaptMean(float* source, float* dest, const uint32_t & width, const uint32_t &height);
    void gapFilter(float* source, float* dest, const uint32_t& width, const uint32_t& height);
    void despeckleFilter(const float* source, float* dest, const uint32_t & width, const uint32_t &height);
    void blockFilter(const float* source, float* dest, const uint32_t & width, const uint32_t &height);
    void publishOutputMaps(const sensor_msgs::ImageConstPtr& inputMsg, const float * dispData,
                           const sensor_msgs::CameraInfoConstPtr& l_info_msg, 
                           const sensor_msgs::CameraInfoConstPtr& r_info_msg);
    void publish_point_cloud(const sensor_msgs::ImageConstPtr& l_image_msg,
                             float * l_disp_data,
                             const sensor_msgs::CameraInfoConstPtr& l_info_msg,
                             const sensor_msgs::CameraInfoConstPtr& r_info_msg);
    
    typedef image_transport::SubscriberFilter Subscriber;
    typedef message_filters::Subscriber<sensor_msgs::CameraInfo> InfoSubscriber;
    typedef message_filters::sync_policies::ExactTime<sensor_msgs::Image, sensor_msgs::Image, sensor_msgs::CameraInfo, sensor_msgs::CameraInfo> ExactPolicy;
    typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Image, sensor_msgs::Image, sensor_msgs::CameraInfo, sensor_msgs::CameraInfo> ApproximatePolicy;
    typedef message_filters::Synchronizer<ExactPolicy> ExactSync;
    typedef message_filters::Synchronizer<ApproximatePolicy> ApproximateSync;
    typedef pcl::PointXYZRGB PointType;
    typedef pcl::PointCloud<PointType> PointCloud;
    
    image_transport::Publisher m_depthImagePub;
    image_transport::Publisher m_disparityImagePub;
    Subscriber m_disparityImageSub;
    Subscriber m_leftImageSub;
    InfoSubscriber m_left_info_sub, m_right_info_sub;

    ros::Publisher m_pointCloudPub;
    
    boost::shared_ptr<ExactSync> m_exact_sync;
    boost::shared_ptr<ApproximateSync> m_approximate_sync;
    
    // Parameters
    bool m_despeckleFilter;
    int m_minSpeckleSegmentSize;
    double m_speckleSimThreshold;
    bool m_adaptiveMeanFilter;
    bool m_gapFilter;
    uint32_t m_threads, m_dispCount;
    bool m_blockFilter;
    int m_blockWidth, m_blockHeight;
    
//     boost::barrier m_barrier;
    boost::mutex m_mutex;
};
    
}